package geops

import (
	"bitbucket.org/kisamoto/geops/geohash"
	"errors"
	"fmt"
	"math"
	"time"
)

var ErrNoDatetime = errors.New("No Datetime associated with point or datetime zero")

type Point struct {
	Lat float64 `json:"lat" msgpack:"lat"`
	Lon float64 `json:"lon" msgpack:"lon"`
	// Optional, can be set later
	GeohashString []byte `json:"geohash,omitempty" msgpack:"geohash,omitempty"`
	// Optional Datetime recorded (for sorting based on time)
	DatetimeRecorded time.Time `json:"datetimeRecorded,omitempty" msgpack:"datetimeRecorded,omitempty"`
	// Optional Owner identifier used to group points to a particular owner
	OwnerIdentifier string `json:"-,omitempty" msgpack:"owner,omitempty"`
}

// Lat = y axis
// Lon = x axis
func NewPoint(lat float64, lon float64) *Point {
	return &Point{Lat: lat, Lon: lon}
}

func NewPointFromArray(latLon [2]float64) *Point {
	// Create a new Point from a [lat, lon] array
	return NewPoint(latLon[0], latLon[1])
}

func NewPointFromGeohash(geohashString []byte) *Point {
	// Create a new Point from a []byte geohash
	return NewPointFromArray(geohash.DecodeGeohash(geohashString))
}

func (p *Point) SetDatetimeRecorded(datetimeRecorded time.Time) {
	p.DatetimeRecorded = datetimeRecorded
}

func (p *Point) SetOwnerIdentifier(id string) {
	p.OwnerIdentifier = id
}

func (p Point) Y() float64 {
	return p.Lat
}

func (p Point) LatRad() float64 {
	// Return point latitude in radians
	return DegToRad(p.Lat)
}

func (p Point) X() float64 {
	return p.Lon
}

func (p Point) LonRad() float64 {
	// Return point longitude in radians
	return DegToRad(p.Lon)
}

func (p Point) ToGeoJSONArray() [2]float64 {
	// Returns array [lon, lat] for GeoJSON
	return [2]float64{p.Lon, p.Lat}
}

func (p1 Point) HaversineDistanceKm(p2 Point) float64 {
	// Haversine Forumla
	lonDistance := p2.LonRad() - p1.LonRad()
	latDistance := p2.LatRad() - p1.LatRad()

	a := math.Pow(math.Sin(latDistance/2), 2) +
		math.Cos(p1.LatRad())*math.Cos(p2.LatRad())*
			math.Pow(math.Sin(lonDistance/2), 2)

	c := 2 * math.Asin(math.Sqrt(a))

	return EARTH_RADIUS_KM * c
}

func (p1 Point) WithinRangeKm(p2 Point, rangeKm float64) bool {
	return p1.HaversineDistanceKm(p2) <= rangeKm
}

func (p Point) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"lat": %v, "lon": %v}`, p.Lat, p.Lon)), nil
}

func (p *Point) Geohash(precisionLength int) []byte {
	if len(p.GeohashString) != (precisionLength + 1) {
		p.GeohashString = geohash.EncodeGeohash(p.Lat, p.Lon, precisionLength)
	}
	return p.GeohashString
}

// Returns true if the point was recorded before the comparison point.
// If datetime is not available return false and an error
func (p1 Point) RecordedBefore(p2 Point) (isBefore bool, err error) {
	if (p1.DatetimeRecorded.IsZero()) || (p2.DatetimeRecorded.IsZero()) {
		err = ErrNoDatetime
	} else {
		isBefore = p1.DatetimeRecorded.Before(p2.DatetimeRecorded)
	}
	return
}

// Returns true if the point was recorded after the comparison point.
// If datetime is not available return false and an error
func (p1 Point) RecordedAfter(p2 Point) (isAfter bool, err error) {
	if (p1.DatetimeRecorded.IsZero()) || (p2.DatetimeRecorded.IsZero()) {
		err = ErrNoDatetime
	} else {
		isAfter = p1.DatetimeRecorded.After(p2.DatetimeRecorded)
	}
	return
}

// Used to compare 2 points if the lat, lon are the same.
// Does not take into account the time the point was recorded
func (p1 Point) EqualLocation(p2 Point) bool {
	return p1.Lat == p2.Lat && p1.Lon == p2.Lon
}

func (p Point) DistanceToLine(l Line) (d Distance) {
	//ToDo: fill this in
	return
}

func DegToRad(degreeNumber float64) float64 {
	return (degreeNumber * math.Pi) / 180
}
