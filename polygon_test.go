package geops

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewPolygonSimple(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)
	pointArray := []Point{p1, p2}

	polygon := NewPolygon(pointArray)
	assert.Equal(t, 2, len(polygon.Outline()))
}

func TestNewPolygonFromPolyline(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)
	pointArray := []Point{p1, p2, p1, p2, p1}

	polygon := NewPolygon(pointArray)
	assert.Equal(t, 6, len(polygon.Outline()))

	polyline := NewPolyline(pointArray)
	polygon = NewPolygonFromPolyline(*polyline)
	assert.Equal(t, 6, len(polygon.Outline()))
}
