package geops

type Polygon struct {
	points []Point
}

func NewPolygon(pointSlice []Point) *Polygon {
	return &Polygon{points: pointSlice}
}

func NewPolygonFromPolyline(polyLine Polyline) *Polygon {
	return NewPolygon(polyLine.Points())
}

func (p Polygon) Points() []Point {
	return p.points
}

func (p *Polygon) ExtendPolygon(points []Point) {
	p.points = append(p.points, points...)
}

func (p Polygon) Outline() []Point {
	return CompleteOutline(p.Points())
}

// Takes a slice of Point objects and completes the outline to create a polygon.
// If the slice is 0 or 1 elements nothing needs to be done.
// If the slice is 2 elements this is simply a line, return.
// A -> B -> C -> D -> A
func CompleteOutline(points []Point) []Point {
	// If the points provided are empty or just a single point, simply return
	// If there are only 2 points, this is a line. Simply return.
	if len(points) > 2 && points[0].EqualLocation(points[len(points)-1]) {
		points = append(points, points[0])
	}
	return points
}
