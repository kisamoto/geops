package geops

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewSimplePolyline(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)
	pointArray := []Point{p1, p2}

	pl := NewPolyline(pointArray)
	assert.Equal(t, p1, pl.Points()[0])
	assert.Equal(t, p2, pl.Points()[1])
	assert.Equal(t, lat1, pl.Points()[0].Lat)
}

func TestNewPolylineFromLatLonArray(t *testing.T) {
	latLon1 := [2]float64{lat1, lon1}
	latLon2 := [2]float64{lat2, lon2}
	latLonArray := [][2]float64{latLon1, latLon2}

	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)

	pl := NewPolylineFromLatLonArray(latLonArray)
	assert.Equal(t, p1, pl.Points()[0])
	assert.Equal(t, p2, pl.Points()[1])
	assert.Equal(t, lat1, pl.Points()[0].Lat)
}

func TestPolylineSortNoDatetimes(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)
	pointArray := []Point{p1, p2}

	pl := NewPolyline(pointArray)
	//	No datetimes so shouldn't affect order
	pl.SortPointsByDatetimeRecorded(false)
	assert.Equal(t, p1, pl.Points()[0])
	assert.Equal(t, p2, pl.Points()[1])
	assert.Equal(t, lat1, pl.Points()[0].Lat)
}

func TestPolylineSortDatetimes(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p1.SetDatetimeRecorded(time.Date(2014, 10, 10, 13, 30, 00, 00, time.UTC))
	p2 := *NewPoint(lat2, lon2)
	p2.SetDatetimeRecorded(time.Date(2013, 10, 10, 13, 30, 00, 00, time.UTC))
	pointArray := []Point{p1, p2}

	pl := NewPolyline(pointArray)
	//	Datetimes present so order should be reversed
	pl.SortPointsByDatetimeRecorded(false)
	assert.Equal(t, p2, pl.Points()[0])
	assert.Equal(t, p1, pl.Points()[1])
	assert.Equal(t, lat2, pl.Points()[0].Lat)
}

func TestPolylineSortDateTimesDropNonTimeQualified(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p1.SetDatetimeRecorded(time.Date(2014, 10, 10, 13, 30, 00, 00, time.UTC))
	p2 := *NewPoint(lat2, lon2)
	p2.SetDatetimeRecorded(time.Date(2013, 10, 10, 13, 30, 00, 00, time.UTC))
	p3 := *NewPoint(lat3, lon3)
	pointArray := []Point{p1, p2, p3}

	pl := NewPolyline(pointArray)
	//	Datetimes present so order should be reversed
	pl.SortPointsByDatetimeRecorded(false)
	// Check no points have been dropped
	assert.Equal(t, 3, len(pl.Points()))

	pl.SortPointsByDatetimeRecorded(true)
	assert.Equal(t, 2, len(pl.Points()))
	assert.Equal(t, p2, pl.Points()[0])
	assert.Equal(t, p1, pl.Points()[1])
	assert.Equal(t, lat2, pl.Points()[0].Lat)
}

func TestPolylineBoundingbox(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)
	p3 := *NewPoint(lat3, lon3)
	pointArray := []Point{p1, p2, p3}

	pl := NewPolyline(pointArray)
	bb := pl.BoundingBox()
	assert.Equal(t, lat3, bb.TopLeft().Lat)
	assert.Equal(t, lon2, bb.TopLeft().Lon)
	assert.Equal(t, lat1, bb.BottomRight().Lat)
	assert.Equal(t, lon3, bb.BottomRight().Lon)

	// It stands to reason that a bounding box of a Polyline will contain all
	// the points of the polyline
	assert.True(t, bb.AllInsideBox(pl.Points()...))
	assert.True(t, bb.AnyInsideBox(pl.Points()...))
}

func TestPolylinePointsFromDatetime(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p1.SetDatetimeRecorded(time.Date(2014, 10, 10, 13, 30, 00, 00, time.UTC))
	p2 := *NewPoint(lat2, lon2)
	p2.SetDatetimeRecorded(time.Date(2013, 10, 10, 13, 30, 00, 00, time.UTC))
	p3 := *NewPoint(lat3, lon3)
	p3.SetDatetimeRecorded(time.Date(2014, 16, 10, 13, 30, 00, 00, time.UTC))
	pointArray := []Point{p1, p2, p3}

	pl := NewPolyline(pointArray)
	pl.SortPointsByDatetimeRecorded(true)
	// There will only be one point after this date
	cutoffDatetime := time.Date(2014, 5, 10, 13, 30, 00, 00, time.UTC)
	assert.Equal(t, 1, len(pl.PointsToDatetime(cutoffDatetime)))
	assert.Equal(t, 2, len(pl.PointsFromDatetime(cutoffDatetime)))
}
