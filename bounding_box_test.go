package geops

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	lat1 = 12.34
	lon1 = 56.78
	lat2 = 78.0
	lon2 = 12.3
	lat3 = 98.76
	lon3 = 65.43
)

func TestNewBoundingBoxSimple(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)

	insidePoint := *NewPoint(50.1, 31.2)

	b, _ := NewBoundingBox(p1, p2)
	assert.True(t, b.AllInsideBox(insidePoint))
	assert.True(t, b.AnyInsideBox(insidePoint))
}

func TestBoundingBoxOneInside(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)

	insidePoint := *NewPoint(50.1, 31.2)
	outsidePoint := *NewPoint(lat3, lon3)

	b, _ := NewBoundingBox(p2, p1)
	assert.False(t, b.AllInsideBox(insidePoint, outsidePoint))
	assert.True(t, b.AnyInsideBox(outsidePoint, insidePoint))
}

func TestNewBoundingBoxLine(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat1, lon2)

	_, err := NewBoundingBox(p1, p2)
	assert.NotNil(t, err)
	assert.Equal(t, ErrLine, err)
}

func TestPolylineIntersectsBoundingBox(t *testing.T) {
	p1 := *NewPoint(lat1, lon1)
	p2 := *NewPoint(lat2, lon2)
	p3 := *NewPoint(50.1, 31.2)
	pointArray := []Point{p1, p2, p3}

	pl := NewPolyline(pointArray)
	b, _ := NewBoundingBox(p1, p2)
	assert.True(t, b.PolylineIntersectsBox(*pl))
	assert.True(t, b.PolylineOnlyInsideBox(*pl))

}
