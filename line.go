package geops

type Line struct {
	start Point
	end   Point

	// Equation of a line Ax + By + C = 0
	gradient float64
}

func NewLine(startPoint Point, endPoint Point) *Line {
	return &Line{start: startPoint, end: endPoint}
}

// Calculate gradient of a line if not already calculated.
// Else 0
func (l *Line) Gradient() (g float64) {
	if l.gradient != 0 && l.end.X() != l.start.X() && l.end.Y() != l.start.Y() {
		topY := l.end.Y() - l.start.Y()
		topX := l.end.X() - l.start.X()
		g = topY / topX
	}
	l.gradient = g
	return
}
