package geops

import (
	"sort"
	"time"
)

type Polyline struct {
	points []Point
}

func NewPolyline(points []Point) *Polyline {
	return &Polyline{points: points}
}

func NewPolylineFromLatLonArray(latLons [][2]float64) *Polyline {
	var pArray []Point
	for i := range latLons {
		pArray = append(pArray, *NewPointFromArray(latLons[i]))
	}
	return NewPolyline(pArray)
}

func NewPolylineFromGeohashArray(geohashes [][]byte) *Polyline {
	var pArray []Point
	for i := range geohashes {
		pArray = append(pArray, *NewPointFromGeohash(geohashes[i]))
	}
	return NewPolyline(pArray)
}

func (pl *Polyline) AddPoint(p Point) *Polyline {
	pl.points = append(pl.points, p)
	return pl
}

func (pl *Polyline) AddPoints(p []Point) *Polyline {
	pl.points = append(pl.points, p...)
	return pl
}

func (pl Polyline) Points() []Point {
	return pl.points
}

// Return a slice of points in the polyline that occur _after_ the given dateTime
func (pl Polyline) PointsFromDatetime(startDatetime time.Time) []Point {
	i := 0
	for _, p := range pl.Points() {
		if p.DatetimeRecorded.IsZero() {
			continue
		}
		if p.DatetimeRecorded.Before(startDatetime) {
			i++
		} else {
			break
		}
	}
	return pl.Points()[i:]
}

// Return a slice of points in the polyline that occur _before_ the given dateTime.
func (pl Polyline) PointsToDatetime(endDatetime time.Time) []Point {
	i := 0
	for _, p := range pl.Points() {
		if p.DatetimeRecorded.IsZero() {
			continue
		}
		if p.DatetimeRecorded.Before(endDatetime) {
			i++
		} else {
			break
		}
	}
	return pl.Points()[:i]

}

// Remove all points from polyline that don't have a datetimeRecorded
func (pl *Polyline) DropNonTimeQualifiedPoints() {
	i := 0
	for _, p := range pl.points {
		if !p.DatetimeRecorded.IsZero() {
			pl.points[i] = p
			i++
		}
	}
	pl.points = pl.points[:i]
}

// Sort the points in the Polyline by the datetimeRecorded attribute.
// If attribute not present will not move order
func (pl *Polyline) SortPointsByDatetimeRecorded(dropNonTimeQualified bool) {
	if dropNonTimeQualified {
		pl.DropNonTimeQualifiedPoints()
	}
	sort.Sort(ByDatetimeRecorded(pl.points))
}

func (pl Polyline) BoundingBox() (bb *BoundingBox) {
	// If we don't have 2 points, can't create a bounding box
	if len(pl.Points()) < 2 {
		return
	}
	lowestLat := pl.Points()[0].Lat
	highestLat := pl.Points()[0].Lat
	lowestLon := pl.Points()[0].Lon
	highestLon := pl.Points()[0].Lon
	for _, p := range pl.points {
		if p.Lat < lowestLat {
			lowestLat = p.Lat
		} else if p.Lat > highestLat {
			highestLat = p.Lat
		}
		if p.Lon < lowestLon {
			lowestLon = p.Lon
		} else if p.Lon > highestLon {
			highestLon = p.Lon
		}
	}
	topLeft := *NewPoint(highestLat, lowestLon)
	bottomRight := *NewPoint(lowestLat, highestLon)
	bb, _ = NewBoundingBox(topLeft, bottomRight)
	return
}

type ByDatetimeRecorded []Point

func (a ByDatetimeRecorded) Len() int {
	return len(a)
}

func (a ByDatetimeRecorded) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a ByDatetimeRecorded) Less(i, j int) bool {
	swap, _ := a[i].RecordedBefore(a[j])
	return swap
}
