# Geops

Open source Go (golang) library that provides tools for working with geospatial
data.

## Point Operations

## Future

* GeoJSON
* GeoHash (with variable accuracy)
* Bearings & speed
* Bounding box
* Polyline
	* Google encoder
* Tools for looking up;
	* Postcodes/Zipcodes;
	* Geocoding with Google/OSM
* DateTime aware
* KML tools
