package geops

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestPointDistance(t *testing.T) {
	const lat1, lon1 = 0.00, 0.00
	const lat2, lon2 = 3.00, 3.00
	const expectedDistanceKm = 471.65228849900194
	const maxRange = 500
	const minRange = 100

	departure := *NewPoint(lat1, lon1)
	arrival := *NewPoint(lat2, lon2)
	actualDistanceKm := departure.HaversineDistanceKm(arrival)

	assert.Equal(t, expectedDistanceKm, actualDistanceKm,
		"Expected distance not correct")

	assert.True(t, departure.WithinRangeKm(arrival, maxRange))
	assert.False(t, departure.WithinRangeKm(arrival, minRange))
}

func TestPointGeoJSON(t *testing.T) {
	trial := *NewPoint(0.00, 1.11)
	expected := [2]float64{1.11, 0.00}
	assert.Equal(t, expected, trial.ToGeoJSONArray())
}

//func TestPointGeohash(t *testing.T) {
//	trial := *NewPoint(78.900, 3.456)
//	expected := `tb85emjh`
//	assert.Equal(t, []byte(expected), trial.Geohash(8))
//}

func TestPointDateTimeBefore(t *testing.T) {
	earlyPoint := NewPoint(12.23, 23.34)
	earlyPoint.SetDatetimeRecorded(time.Date(2013, 10, 10, 13, 30, 00, 00, time.UTC))
	earlyPointRef := *earlyPoint

	latePoint := NewPoint(23.34, 45.56)
	latePoint.SetDatetimeRecorded(time.Date(2014, 10, 10, 13, 30, 00, 00, time.UTC))
	latePointRef := *latePoint

	isBefore, _ := earlyPoint.RecordedBefore(latePointRef)
	isAfter, _ := earlyPoint.RecordedAfter(latePointRef)

	assert.True(t, isBefore)
	assert.False(t, isAfter)

	isBefore, _ = latePoint.RecordedBefore(earlyPointRef)
	isAfter, _ = latePoint.RecordedAfter(earlyPointRef)

	assert.False(t, isBefore)
	assert.True(t, isAfter)
}

func TestPointNoDateTime(t *testing.T) {
	earlyPoint := *NewPoint(12.23, 23.34)
	latePoint := *NewPoint(23.34, 45.56)

	isBefore, err1 := earlyPoint.RecordedBefore(latePoint)
	isAfter, err2 := earlyPoint.RecordedAfter(latePoint)

	assert.False(t, isBefore)
	assert.False(t, isAfter)

	assert.Equal(t, ErrNoDatetime, err1)
	assert.Equal(t, ErrNoDatetime, err2)

}

func TestPointEquality(t *testing.T) {
	p1 := NewPoint(12.23, 23.34)
	p2 := NewPoint(12.23, 23.34)
	assert.True(t, p1.EqualLocation(*p2))
}
