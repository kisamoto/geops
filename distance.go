package geops

type Distance struct {
	miles  float64
	meters float64
}

func NewMilesDistance(distanceMiles float64) (distance *Distance) {
	distance.SetDistanceMiles(distanceMiles)
	return
}

func NewMetersDistance(distanceMeters float64) (distance *Distance) {
	distance.SetDistanceMeters(distanceMeters)
	return
}

func (d Distance) Miles() float64 {
	return d.miles
}

func (d Distance) Meters() float64 {
	return d.meters
}

func (d *Distance) SetDistanceMeters(metersDistance float64) {
	d.meters = metersDistance
	d.miles = (metersDistance / METERS_PER_MILE)
}

func (d *Distance) SetDistanceMiles(milesDistance float64) {
	d.miles = milesDistance
	d.meters = (milesDistance * METERS_PER_MILE)
}
