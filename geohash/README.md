# Geops/Geohash

Open source geohash package for Go (golang).

Easy to use:

    $ go get bitbucket.org/kisamoto/geops

And import in the `.go` file:

    import (
		"bitbucket.org/kisamoto/geops/geohash"
		)

	// Will geohash lat, lon with precision '8' (see table below for km error)
	geohashedCoordinates := geohash.EncodeGeohash(1.11, 50.50, 8)

## Encoder

	geohash.EncodeGeohash(lat float64, lon float64, precisionLength int) []byte

## Decoder

	geohash.DecodeGeohash(geohash []byte) [2]float64

## Precision

|geohash length	|lat bits|lng bits|lat error|lng error|km error|
|:-------------:|:------:|:------:|:-------:|:-------:|:------:|
|1|2|3| 	±23 		|±23		|±2500|
|2|5|5| 	± 2.8 		|± 5.6 		|±630|
|3|7|8| 	± 0.70 		|± 0.7 		|±78|
|4|10|10| 	± 0.087 	|± 0.18 	|±20|
|5|12|13| 	± 0.022 	|± 0.022 	|±2.4|
|6|15|15| 	± 0.0027 	|± 0.0055 	|±0.61|
|7|17|18| 	±0.00068 	|±0.00068 	|±0.076|
|8|20|20| 	±0.000085 	|±0.00017 	|±0.019|

More information can be found on Geohashes on [Wikipedia](http://en.wikipedia.org/wiki/Geohash)
