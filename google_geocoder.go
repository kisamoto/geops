package geops

import (
	"fmt"
	"strings"
)

type GoogleGeocoder struct {
	apiKey string

	// Optional components specified in Geocoding API spec
	bounds     BoundingBox
	language   string
	region     string
	components map[string]string
}

func NewGoogleGeocoder(apiKey string) *GoogleGeocoder {
	return &GoogleGeocoder{apiKey: apiKey}
}

func (g *GoogleGeocoder) SetBounds(boundingbox BoundingBox) {
	g.bounds = boundingbox
}

func (g *GoogleGeocoder) SetLanguage(language string) {
	g.language = language
}

func (g *GoogleGeocoder) SetRegion(region string) {
	g.region = region
}

func (g *GoogleGeocoder) SetComponents(components map[string]string) {
	g.components = components
}

func (g GoogleGeocoder) FetchResponse(address string) (resp []byte) {
	parameters := []GETParam{
		GETParam{key: "address", value: address},
		GETParam{key: "key", value: g.apiKey}}
	if g.language != "" {
		parameters = append(parameters, GETParam{key: "language", value: g.language})
	}
	if g.region != "" {
		parameters = append(parameters, GETParam{key: "region", value: g.region})
	}
	if len(g.components) == 0 {
		parameters = append(parameters, GETParam{key: "components", value: g.BuildComponents()})
	}
	if &g.bounds != nil {
		parameters = append(parameters, GETParam{key: "bounds", value: g.BuildBoundingBox()})
	}
	resp = GETRequest(GOOGLE_GEOCODING_URL, parameters...)
	return
}

func (g GoogleGeocoder) Geocode(a Address) Point {
	resp := g.FetchResponse(a.address)
	fmt.Println(string(resp))
	return Point{}
}

func (g GoogleGeocoder) BuildComponents() (componentString string) {
	cArray := []string{}
	for key, value := range g.components {
		cArray = append(cArray, key+":"+value)
	}
	componentString = strings.Join(cArray, "|")
	return
}

func (g GoogleGeocoder) BuildBoundingBox() (boundingBoxString string) {
	boundingBoxString = fmt.Sprintf("%.8f,%.8f|%.8f,%.8f",
		g.bounds.TopLeft().Lat,
		g.bounds.TopLeft().Lon,
		g.bounds.BottomRight().Lat,
		g.bounds.BottomRight().Lon)
	return
}
