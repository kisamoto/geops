package geops

import (
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type Geocoder interface {
	Geocode(a Address) Point
}

type GETParam struct {
	key   string
	value string
}

func BuildGetParams(urlString string, params ...GETParam) (baseUrl *url.URL) {
	baseURL, err := url.Parse(urlString)
	HandleError(err)
	baseParams := url.Values{}
	for _, p := range params {
		baseParams.Add(p.key, p.value)
	}
	baseURL.RawQuery = baseParams.Encode()
	return
}

func GETRequest(urlString string, params ...GETParam) (body []byte) {
	baseUrl := BuildGetParams(urlString, params...)
	resp, err := http.Get(baseUrl.String())
	HandleError(err)
	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	HandleError(err)

	return
}

func HandleError(err error) {
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
}
