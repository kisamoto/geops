package geodb

import (
	"bitbucket.org/kisamoto/geops"
)

// Pass in an existing mongo connection and define lat/lon (or GeoJSON) fields and
// have the queries executed
type MongoDB struct {

}

func (db *MongoDB) UseConnection(connection interface {}) {

}

func (db MongoDB) ClosestPoint(p geops.Point) geops.Point {
	return geops.Point{}
}

func (db MongoDB) GeoNear(p geops.Point, distanceKm uint64) []geops.Point {
	return []geops.Point{}
}
