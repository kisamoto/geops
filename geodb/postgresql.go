package geodb

import (
	"bitbucket.org/kisamoto/geops"
)

// Pass in an existing PostgreSQL connection (with PostGIS).
// Define lat/lon columns and have the queries run for you.
type PostgreSQL struct {

}

func (db *PostgreSQL) UseConnection(connection interface {}) {

}

func (db PostgreSQL) ClosestPoint(p geops.Point) geops.Point {
	return geops.Point{}
}

func (db PostgreSQL) GeoNear(p geops.Point, distanceKm uint64) []geops.Point {
	return []geops.Point{}
}
