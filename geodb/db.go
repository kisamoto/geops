package geodb

import (
	"bitbucket.org/kisamoto/geops"
)

type Database interface {
	UseConnection(connection interface {})
	// Return the closest Point object to a given Point object
	ClosestPoint(p geops.Point) geops.Point
	// Return an array of Point objects within 'x' KM of a given Point
	// Sort points by distance, closest first
	GeoNear(p geops.Point, distanceKm uint64) []geops.Point
}
