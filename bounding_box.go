package geops

import (
	"errors"
)

var ErrLine = errors.New("Points on same axis. This is a line, not a bounding box")

type BoundingBox struct {
	topLeft     Point
	bottomRight Point
}

func NewBoundingBox(p1 Point, p2 Point) (b *BoundingBox, err error) {
	if p1.Lat == p2.Lat || p1.Lon == p2.Lon {
		err = ErrLine
	}
	topLeft, bottomRight := CalculateTopLeftBottomRight(p1, p2)
	b = &BoundingBox{topLeft: *topLeft, bottomRight: *bottomRight}
	return
}

// Takes any number of Point objects and returns true only if all points
// are within the BoundingBox
func (bb BoundingBox) AllInsideBox(points ...Point) (allInside bool) {
	for _, p := range points {
		if !InsideBoundBox(bb.topLeft, bb.bottomRight, p) {
			// If any point is not in the bounding box, return false.
			return
		}
	}
	allInside = true
	return
}

func (bb BoundingBox) AnyInsideBox(points ...Point) (anyInside bool) {
	for _, p := range points {
		if InsideBoundBox(bb.topLeft, bb.bottomRight, p) {
			// If any point is inside the bounding box, return true
			anyInside = true
			return
		}
	}
	return
}

func (bb BoundingBox) TopLeft() Point {
	return bb.topLeft
}

func (bb BoundingBox) BottomRight() Point {
	return bb.bottomRight
}

func (bb *BoundingBox) SetTopLeft(tl Point) {
	bb.topLeft = tl
}

func (bb *BoundingBox) SetBottomRight(br Point) {
	bb.bottomRight = br
}

func (bb BoundingBox) PolylineIntersectsBox(pl Polyline) bool {
	return bb.AnyInsideBox(pl.points...)
}

func (bb BoundingBox) PolylineOnlyInsideBox(pl Polyline) bool {
	return bb.AllInsideBox(pl.points...)
}

func InsideBoundBox(topLeft Point, bottomRight Point, p Point) bool {
	insideTopLeft := topLeft.Lon <= p.Lon && topLeft.Lat >= p.Lat
	insideBottomRight := bottomRight.Lon >= p.Lon && bottomRight.Lat <= p.Lat
	return insideTopLeft && insideBottomRight
}

func CalculateTopLeftBottomRight(p1 Point, p2 Point) (topLeft *Point, bottomRight *Point) {
	var topLeftLat, topLeftLon float64
	var bottomRightLat, bottomRightLon float64

	if p1.Lon > p2.Lon {
		bottomRightLon = p1.Lon
		topLeftLon = p2.Lon
	} else {
		bottomRightLon = p2.Lon
		topLeftLon = p1.Lon
	}
	if p1.Lat > p2.Lat {
		topLeftLat = p1.Lat
		bottomRightLat = p2.Lat
	} else {
		bottomRightLat = p1.Lat
		topLeftLat = p2.Lat
	}
	return NewPoint(topLeftLat, topLeftLon), NewPoint(bottomRightLat, bottomRightLon)
}
