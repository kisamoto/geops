package geops

// Numerical constants
const (
	EARTH_RADIUS_KM = 6371
	METERS_PER_MILE = 1609
)

// API urls
const (
	GOOGLE_GEOCODING_URL = "https://maps.googleapis.com/maps/api/geocode/json"
)
